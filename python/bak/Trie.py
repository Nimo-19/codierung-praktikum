import collections
from arith import Arith

class Node:
    def __init__(self,char,low_count=0,lower_ctx = None, isEsc = False):
        self.char = char
        self.count = 1
        self.total= 1
        self.nodeLists = []
        self.lower_ctx = lower_ctx
        self.ctx_size = 0
        if not isEsc:
            self.nodeLists.append(Node('ESC', isEsc = True))
        self.low_count = low_count
        self.high_count = self.low_count + 1
    
    def addNode(self,node):
        hc = self.nodeLists[-1:][0].high_count
        self.nodeLists.append(node)
        node.low_count = hc
        node.high_count = hc+1
        self.total+= 1
        node.ctx_size = self.ctx_size + 1

    def addLowerCtx(self,node):
        self.lower_ctx = node

    def plus(self, char):
        #node = self.next(char)
        done = False
        for node in self.nodeLists:
            if node.char == char:
                node.count += 1
                node.high_count += 1
                done = True
            elif done:
                node.low_count += 1
                node.high_count += 1

        self.total += 1

    def getCodingValues(self,char):
        node = self.next(char)
        return node.low_count, node.high_count, self.total

    def isLeaf(self):
        return len(self.nodeLists) == 0

    def next(self, char):
        for node in self.nodeLists:
            if node.char == char:
                return node
        return None

    def hasNode(self,char):
        for node in self.nodeLists:
            if node.char == char:
                return True
        return False        

    def findValue(self,value):
        if value == self.total:
            node = self.nodeLists[-1]
            return node.char, node.low_count, node.high_count
        for node in self.nodeLists:
           #print 'currentNode: '+ str(node)
            if node.low_count <= value and node.high_count > value:
                return node.char, node.low_count, node.high_count
       #print 'Error'
       #print 'total: '+str(self.total)

    def __str__(self):
        return '['+str(self.char)+' : '+str(self.count)+'; lc: '+str(self.low_count)+', hc: '+str(self.high_count)+']'


class Trie:
    def __init__(self,ctx_len):
        self.root = Node('Root')
        self.ctx_len = ctx_len
        self.minus_ctx = Node('minus_ctx')
        self.arith = Arith()
        for i in range(0,256):
            self.minus_ctx.nodeLists.append(Node(i,low_count=i+1))
            self.minus_ctx.total += 1
        self.minus_ctx.addNode(Node('END'))

    def path(self,string):
        node = self.root
        if string == '':
            return self.root
        for char in string:
            if node.hasNode(char):
                node = node.next(char)
            else:
                return None
        return node 

    def addChar(self,ctx,char,workingNode = None):
        node = self.path(ctx)
        if node.hasNode(char):
            node.plus(char)
            #node = node.next(char)
            #node.count += 1
            # updaten der niedrigeren Kontexte
            #while node.lower_ctx != None:
            #    node = node.lower_ctx
            #    node.count += 1
            #    ctx = ctx[1:]
            if(not workingNode is None):
                workingNode.addLowerCtx(node)
            if ctx != '':
                self.addChar(ctx[1:],char,workingNode)
        else:
            new_node= Node(char)
           #print "new Node: "+str(new_node)
            node.addNode(new_node);
            if(not workingNode is None):
                workingNode.addLowerCtx(new_node)
            if ctx != '':
                self.addChar(ctx[1:],char,new_node)

    def read(self,input):
        test = self.minus_ctx.next('END')
       #print 'END-Symb'
       #print 'low_count: ' + str(test.low_count)
       #print 'high_count: ' +  str(test.high_count)
        for i in range(0, len(input)):
            if i < self.ctx_len:
               #print input[0:i] + " : " + input[i]
                #codieren
                self.read_trie(input[0:i:],input[i])
                # Trie updaten
                self.addChar(input[0:i:],input[i])
            else:
               #print input[i-self.ctx_len:i] + " : " + input[i]
                #codieren
                bla = self.read_trie(input[i-self.ctx_len:i:],input[i])
                if bla == 'WA':
                    print 'WAAAAAAA'
                    break
                # Trie updaten
                self.addChar(input[i-self.ctx_len:i:],input[i])
        self.read_trie(input[len(input)-self.ctx_len:len(input):],'END')
        self.arith.end_encode()

    def read_trie(self,ctx,char):

        node = self.path(ctx)
        while not node.hasNode(char):
            #escape zeichen codieren
           #print('Encode: ESC')
            low_count, high_count, total = node.getCodingValues('ESC')
            bla = self.arith.encode(low_count,high_count,total)
            if bla == 'WA':
                return bla
            if ctx == '':
                # zu Kontext -1 springen
                node = self.minus_ctx
                break
            ctx = ctx[1:]
            node = self.path(ctx)
        # Hier codieren
        # encode_next(node,char)
       #print('Encode: '+ char)
        if node.char == 'minus_ctx':
            if(char == 'END'):
                low_count, high_count, total = node.getCodingValues(char)
                self.arith.encode(low_count,high_count,total)
            else:
               #print 'Encode: '+ str(ord(char))
                low_count, high_count, total = node.getCodingValues(ord(char))
                self.arith.encode(low_count,high_count,total)
        else:
           #print('Encode: '+ char)
            low_count, high_count, total = node.getCodingValues(char)
            self.arith.encode(low_count,high_count,total)

    def get_ctx(self,i,ctx = '', node = None):
        if node == None:
            node = self.path(ctx)
        if i>self.ctx_len:
            return None
        if i == 0:
            c = collections.Counter()
            for n in node.nodeLists:
                c += collections.Counter({n.char : n.count})
            return c
        else:
            c = collections.Counter()
            for n in node.nodeLists:
                c += self.get_ctx(i-1,n)
            return c

    def readCode(self,input,orig):
        self.arith.out = input
        ctx = ''
        currentNode = self.root
        escaped = False;
        self.arith.getBit(self.arith.buffersize)
        decmsg = ''
        #while ardec.out != '' and ardec.mBuffer != 64:
        
        escaped_test_coutner = 0

        test = self.minus_ctx.next('END')

       #print 'END-Symb'
       #print 'low_count: ' + str(test.low_count)
       #print 'high_count: ' +  str(test.high_count)

       #print 'start DECODING'

        counter = 0
        while True:
            #print 'currentNode: '+ str(currentNode)
            value, mStep = self.arith.decode_target(currentNode.total)
           #print 'value: '+str(value)
            if value > currentNode.total:
               #print 'ERROR' 
                break

            symbol, low_count, high_count = currentNode.findValue(value)
            self.arith.decode(low_count,high_count,mStep)

            #print 'test_sym: '+str(symbol)

            if symbol == 'END':
               #print 'symbol: '+symbol
                break
            elif symbol == 'ESC':
               #print 'symbol: '+symbol
                escaped_test_coutner += 1
                escaped = True
                if ctx == '':
                    currentNode = self.minus_ctx
                else:
                    ctx = ctx[1:]
                    currentNode = self.path(ctx)
            else:
                if isinstance(symbol,int):
                    symbol = chr(symbol)
               #print 'symbol: '+symbol
               ##print 'old ctx ' + ctx
                decmsg += symbol
                if decmsg[counter] != orig[counter]:
                   #print 'ctx: '+ctx
                   #print "ERROR nicht gleich"
                    node = self.root
                    for l in ctx:
                        node = node.next(l)
                       #print 'node: '+ str(node)
                       #print 'childs:'
                        for l in node.nodeLists:
                           print str(l.char)
                        if node == None:
                            break

                    return decmsg
                    break
                counter += 1
                ctx += symbol
               ##print 'in between ctx ' + ctx
                if escaped:
                    escaped_test_coutner = 0
                    ctx = decmsg[-self.ctx_len:]
                    escaped = False
               ##print 'decmsg: '+decmsg
               ##print 'add Node Ctx: '+decmsg[-self.ctx_len-1:-1]
                self.addChar(decmsg[-self.ctx_len-1:-1],symbol)
                if len(ctx) > self.ctx_len:
                    ctx = ctx[1:]
                currentNode = self.path(ctx)
               ##print 'current ctx ' + ctx
            if currentNode == None:
               #print 'ERROR'
               #print ctx
                node = self.root
                for l in ctx:
                    if node.has(l):
                       #print str(node)
                        node = node.next(l)
                    else:
                       #print 'ERROR'
                        break
                break
            if escaped_test_coutner > self.ctx_len+1:
               #print 'bla too much escapes'
                break
        #print decmsg
        return decmsg

           
if __name__ == "__main__":
    ctx_len = 2
    lorem = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut l"
    lorem2 = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,"
    abab = "abc"*40
    lorem3 = "Lorem "*10
    msg = abab
    trie = Trie(ctx_len)
   #print 'startenc'
    trie.read(msg)
    msg_bin = ''.join(format(ord(x), 'b') for x in msg)
    endcoded = trie.arith.out
   #print(trie.arith.out)
   #print(len(trie.arith.out))
    #print(msg_bin)
   #print(len(msg_bin))
    triedec = Trie(ctx_len)
   #print 'startdec'
    decoded_msg = triedec.readCode(endcoded,msg)
    if decoded_msg == msg:
       print 'YEEEEEEEEEEAAAAAAAHHHHHHH'
    bpc = float(len(endcoded))/float(len(msg))
    print('Encoded_len: '+str(len(trie.arith.out)))
    print('Unicode_len: ' +str(len(msg_bin)))
    print('bpc :'+str(bpc))

