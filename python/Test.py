from Trie import Trie
import sys

if __name__ == '__main__':
    print sys.argv
    
    start = 4
    end = 5

    if len(sys.argv) == 2:
        start = int(sys.argv[1])
        end = start + 1
    elif len(sys.argv) == 3:
        start = int(sys.argv[1])
        end = int(sys.argv[2])

    for i in range(start,end):
        ctx_len = i
        enc_Trie = Trie(ctx_len,True)
        input = ""
        linecounter = 1000
        for line in open('data/Lewis_Carroll_Alices_Adventures_in_Wonderland.txt'):
            input += line
            linecounter -= 1
            #if linecounter == 0:
            #    break;
        print '+'*20
        print 'Context Length: '+str(i)
        print '+'*20
        print 'startenc'
        print '+'*20
        encoded = enc_Trie.encode(input)
        dec_Trie = Trie(ctx_len,True)
        print '+'*20
        print 'startdec'
        print '+'*20
        decoded = dec_Trie.decode(encoded,input)
        bps = float(len(encoded))/float(len(input))
        print '+'*20
        print 'lenght endcoded: '+ str(len(encoded))
        print 'charcount input: '+ str(len(input))
        print 'bps : '+ str(bps)
        print '+'*20
        if(input == decoded):
            print 'Correctly Decoded'
        else:
            print 'ERROR: not Correctly Decoded'

