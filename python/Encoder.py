# -*- coding: utf-8 -*-

from Trie import Trie
import bitstring
import sys

class Encoder:
    def __init__(self,input_path,output_path,ctx_len=4,exculsion=True,esc_method=3):
        self.trie = Trie(ctx_len, exculsion, esc_method,extrachars=['th','Alice','said'])
        self.input_path = input_path
        self.output_path= output_path
        self.input = []
        self.output = ''
        self.input_len = 0
        self.processFile()
        #print self.input[:100]

    def processFile(self):
        for line in open(self.input_path):
            counter = 0
            while counter < len(line):
                currentchar = line[counter]
                if currentchar == 't' and line[counter+1] == 'h':
                    self.input.append('th')
                    counter += 2
                elif currentchar == 'A' and line[counter+1:counter+5] == 'lice':
                    self.input.append('Alice')
                    counter += 5
                elif currentchar == 's' and line[counter+1:counter+4] == 'aid':
                    self.input.append('said')
                    counter += 4
                else:
                    self.input.append(currentchar)
                    counter += 1
            self.input_len += counter

    def out_to_file(self):
        bin_out = '0b'+self.output
        bitarray = bitstring.BitArray(bin=bin_out)
        f = open(self.output_path,'wb')
        bitarray.tofile(f)
        f.close()
        
    def encode(self):
        print '+'*20
        print 'start Encoding'
        print '+'*20
        self.output = self.trie.encode(self.input)
        print '+'*20
        print 'Encoding Finished'
        print 'Write File'
        print '+'*20
        self.out_to_file()

if __name__ == '__main__':
    print sys.argv

    if len(sys.argv) < 3:
        print 'Input and output Path needed'
    else:
        encoder = Encoder(sys.argv[1],sys.argv[2])
        encoder.encode()
        bpc = float(len(encoder.output))/float(encoder.input_len)
        print str(bpc)+' bpc'



