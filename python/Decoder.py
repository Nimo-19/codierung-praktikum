# -*- coding: utf-8 -*-

from Trie import Trie
import bitstring
import sys

class Decoder:
    def __init__(self,input_path,output_path,orig_path=None,ctx_len=4,exculsion=True,esc_method=3):
        self.trie = Trie(ctx_len, exculsion, esc_method,extrachars=['th','Alice','said'])
        self.input_path = input_path
        self.output_path= output_path
        self.orig_path = orig_path
        self.orig = ''
        self.input = ''
        self.output = ''
        self.processFile()

    def processFile(self):
        bits = bitstring.Bits(filename=self.input_path)

        #print bits[300000]
        self.input = bits.bin
        #test = list(self.input)
        #test[300000] = "1"
        #self.input = "".join(test)

        if not self.orig_path is None:
            for line in open(self.orig_path):
                self.orig += line
            

    def out_to_file(self):
        f = open(self.output_path,'wb')
        f.write(self.output)
        f.close()
        
    def decode(self):
        print '+'*20
        print 'start Decoding'
        print '+'*20
        if self.orig_path is None:
            self.output = self.trie.decode(self.input)
        else:
            self.output = self.trie.decode(self.input,self.orig)
        print '+'*20
        print 'Deccoding Finished'
        print 'Write File'
        print '+'*20
        self.out_to_file()

if __name__ == '__main__':
    print sys.argv

    if len(sys.argv) < 3:
        print 'Input and output Path needed'
    elif len(sys.argv) == 4:
        decoder = Decoder(sys.argv[1],sys.argv[2],orig_path=sys.argv[3])
        decoder.decode()
    else:
        decoder = Decoder(sys.argv[1],sys.argv[2])
        decoder.decode()



