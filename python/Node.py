# -*- coding: utf-8 -*-

class Node:
    def __init__(self,char,ctx_len=0,low_count=0,lower_ctx = None, isEsc = False):
        # Buchtabe des Knotens
        self.char = char
        # Häufigkeit des Knotens
        self.count = 1
        # Summe der häufigkeiten der Kindknoten
        self.total= 1
        # Liste der Kind-Knoten
        self.nodeLists = []
        # "Link" zu niedrigerem Kontext
        self.lower_ctx = lower_ctx
        if not isEsc:
            # Escape Zeichen zu den Kindknoten hinzufügen
            self.nodeLists.append(Node('ESC', isEsc = True))
        # Low und High Count für Arithmetischen Codierer
        self.low_count = low_count
        self.high_count = self.low_count + 1
        # Liste der Zeichen der Kindknoten (für Exklusion)
        self.valueList = []
        # Höchster Count in Kindknoten (für 3. Escape-Methode)
        self.highest_count = 1
        # Kontext-Order des Knoten (für 3. Escape-Methode)
        self.order = 0
        # Kontext Größe des Dekodieres (für 3. Escape-Methode)
        self.ctx_len = ctx_len
    
    def addNode(self,node):
        hc = self.nodeLists[-1:][0].high_count
        self.nodeLists.append(node)
        node.low_count = hc
        node.high_count = hc+1
        self.total+= 1
        self.valueList.append(node.char)
        node.order = self.order + 1 

    def removeNodeByValue(self,char):
        found = False
        removeNode = None;
        for node in self.nodeLists:
            if not found and node.char == char:
                removeNode = node;
                found = True
            elif found:
                node.low_count -= removeNode.count
                node.high_count -= removeNode.count
        if found:
            self.total -= removeNode.count
            self.nodeLists.remove(removeNode)
            self.valueList.remove(removeNode.char)
        else:
            print("ERROR: not found: "+str(char))

    # Escape Mehtode 1: Incrementiern wenn eine Häufigkeit incrementiert wird
    def compute_esc_1(self):
        self.plus('ESC')

    # Escape Mehtode 2: Formel auf Basis der möglichen und vorhandenen Zeichen
    def compute_esc_2(self):
        sum_symbol = len(self.nodeLists)-1
        esc_count = ((256 - sum_symbol)*sum_symbol)/(256*self.highest_count)
        if esc_count < 1:
            esc_count = 1
        sum_count = esc_count
        for node in self.nodeLists:
            if node.char == 'ESC':
                node.count = esc_count
                node.high_count = esc_count
            else:
                node.low_count = sum_count
                sum_count += node.count
                node.high_count = sum_count

        self.total = sum_count

    # Escape Mehtode 3: ähnlich wie methode 2, aber mit der Order als Korrekturfaktor, je kleiner die Order desto kleiner die ESC Wahrscheinlichket
    def compute_esc_3(self):
        sum_symbol = len(self.nodeLists)-1
        esc_count = ((256 - sum_symbol)*sum_symbol)/(256*self.highest_count)
        if self.ctx_len > self.order:
            esc_count /= (self.ctx_len-self.order)
        if esc_count < 1:
            esc_count = 1
        sum_count = esc_count
        for node in self.nodeLists:
            if node.char == 'ESC':
                node.count = esc_count
                node.high_count = esc_count
            else:
                node.low_count = sum_count
                sum_count += node.count
                node.high_count = sum_count

        self.total = sum_count


    def addLowerCtx(self,node):
        self.lower_ctx = node

    #Incrementieren der Häufigkeit, des zu char entsprechenden Knotens inkl. aktualisieren der Low und Highcounts
    def plus(self, char):
        #node = self.next(char)
        done = False
        for node in self.nodeLists:
            if node.char == char:
                node.count += 1
                node.high_count += 1
                done = True
                if node.count > self.highest_count:
                    self.highest_count = node.count
            elif done:
                node.low_count += 1
                node.high_count += 1

        self.total += 1

    #Low- , High-Count und Total für die Arithmentischen Codierer
    def getCodingValues(self,char):
        node = self.next(char)
        return node.low_count, node.high_count, self.total

    def isLeaf(self):
        return len(self.nodeLists) == 0

    # Nächster Knoten des Baums
    def next(self, char):
        for node in self.nodeLists:
            if node.char == char:
                return node
        return None

    def hasNode(self,char):
        for node in self.nodeLists:
            if node.char == char:
                return True
        return False        

    # Buchstabe aufgrund des vom arithmetischen Dekodieres berechneten Wertes
    def findValue(self,value):
        if value == self.total:
            node = self.nodeLists[-1]
            return node.char, node.low_count, node.high_count
        for node in self.nodeLists:
            if node.low_count <= value and node.high_count > value:
                return node.char, node.low_count, node.high_count

    def __str__(self):
        return '['+str(self.char)+' : '+str(self.count)+'; lc: '+str(self.low_count)+', hc: '+str(self.high_count)+']'
    def __repr__(self):
        return str(self)

