# -*- coding: utf-8 -*-

import collections
from arith import Arith
from Node import Node

class Trie:
    def __init__(self,ctx_len,exculsion=False,esc_method=3,extrachars=[]):
        # Kontext Länge
        self.ctx_len = ctx_len
        # Wurzelknoten
        self.root = Node('Root',ctx_len = self.ctx_len)
        # Kontext -1
        self.minus_ctx = Node('minus_ctx',ctx_len = self.ctx_len,isEsc=True)
        for i in range(0,256):
            char = chr(i)
            self.minus_ctx.nodeLists.append(Node(char,ctx_len = self.ctx_len,low_count=i+1))
            self.minus_ctx.valueList.append(char)
            self.minus_ctx.total += 1
        counter = 256
        print "Zusätzliche Zeichen, neben Unicode:"
        for char in extrachars:
            print char
            self.minus_ctx.nodeLists.append(Node(char,ctx_len = self.ctx_len,low_count=counter+1))
            self.minus_ctx.valueList.append(char)
            self.minus_ctx.total += 1
            counter += 1
        self.minus_ctx.addNode(Node('END',ctx_len = self.ctx_len,low_count=counter+1))
        self.exculsion = exculsion
        self.esc_method = esc_method
        # Aritmetischer Codierer
        self.coder= Arith()

    def path(self,path):
        node = self.root
        if not path:
            return self.root
        for char in path:
            if node.hasNode(char):
                node = node.next(char)
            else:
                return None
        return node 

    # Hinzufügen einen neuen Buchstaben
    def addChar(self,ctx,char,workingNode = None):
        node = self.path(ctx)
        # Hinzufügen als incrementierte Häufigkeit
        if node.hasNode(char):
            node.plus(char)

            if self.esc_method == 1:
                node.compute_esc_2()
            elif self.esc_method == 2:
                node.compute_esc_2()
            elif self.esc_method == 3:
                node.compute_esc_3()

            if(not workingNode is None):
                workingNode.addLowerCtx(node)
            if ctx:
                self.addChar(ctx[1:],char,workingNode)
        # Hinzufügen als neuer Knoten
        else:
            new_node= Node(char,ctx_len = self.ctx_len)
            node.addNode(new_node);

            if self.esc_method == 1:
                node.compute_esc_1()
            elif self.esc_method == 2:
                node.compute_esc_2()
            elif self.esc_method == 3:
                node.compute_esc_2()

            if(not workingNode is None):
                workingNode.addLowerCtx(new_node)
            if ctx:
                self.addChar(ctx[1:],char,new_node)

    # Weglassen von bereits in höheren Kontexten ausgeschlossenen Zeichen (Codierer)
    def exclude(self,node,char,excludeList):
        total = 0
        low_count = 0
        high_count = 0
        found = False
        for n in node.nodeLists:
            if not n.char in excludeList:
                total += n.count;
                if n.char == char:
                    found = True
                    high_count += n.count
                elif not found:
                    low_count+=n.count
                    high_count+=n.count
        return low_count, high_count, total

    # Streichen breits gesehener Zeichen aus dem Kontext -1
    def updateMinusCtx(self,charNumber):
        self.minus_ctx.removeNodeByValue(charNumber)

    #Codieren eines Inputs
    def encode(self,input):
        steps = len(input)/10
        steps_perc = len(input)/100
        for i in range(0, len(input)):
            if i%steps == 0:
                print str(i/steps_perc) + "% ..."
            if i < self.ctx_len:
                #codieren
                self.encode_char(input[0:i:],input[i])
                # Trie updaten
                self.addChar(input[0:i:],input[i])
            else:
                #codieren
                self.encode_char(input[i-self.ctx_len:i:],input[i])
                # Trie updaten
                self.addChar(input[i-self.ctx_len:i:],input[i])
        # END-Symbol codieren
        self.encode_char(input[len(input)-self.ctx_len:len(input):],'END')
        self.coder.end_encode()
        return self.coder.out

    #Codieren eines Zeichens 
    def encode_char(self,ctx,char):

        node = self.path(ctx)
        excludeList = []
        while not node.hasNode(char):
            #escape zeichen codieren
            if self.exculsion and excludeList:
                low_count, high_count, total = self.exclude(node,'ESC',excludeList)
                if total > node.next('ESC').count:
                    self.coder.encode(low_count,high_count,total)
            else:
                low_count, high_count, total = node.getCodingValues('ESC')
                bla = self.coder.encode(low_count,high_count,total)
                if bla == 'WA':
                    return bla
            if not ctx:
                # zu Kontext -1 springen
                node = self.minus_ctx
                break
            excludeList = list(set(node.valueList + excludeList))
            ctx = ctx[1:]
            node = self.path(ctx)
        if node.char == 'minus_ctx':
            if(char == 'END'):
                low_count, high_count, total = node.getCodingValues(char)
                self.coder.encode(low_count,high_count,total)
            else:
                low_count, high_count, total = node.getCodingValues(char)
                self.coder.encode(low_count,high_count,total)
                #smybol aus dem minux-context rauswerfen
                self.updateMinusCtx(char)
        else:

            if self.exculsion and excludeList:
                low_count, high_count, total = self.exclude(node,char,excludeList)
                self.coder.encode(low_count,high_count,total)
            else:
                low_count, high_count, total = node.getCodingValues(char)
                self.coder.encode(low_count,high_count,total)

    # Weglassen von bereits in höheren Kontexten ausgeschlossenen Zeichen (Dekodierer)
    def excludeDecode(self,currentNode,excludeList):
        total = 0
        for n in currentNode.nodeLists:
            if n.char not in excludeList:
                total += n.count
        if total == 1:
            return 'ESC',0,1,1,1
        value, mStep = self.coder.decode_target(total)
        low_count = 0
        high_count = 0
        symbol = None
        for n in currentNode.nodeLists:
            if n.char not in excludeList:
                if low_count <= value and high_count+n.count > value:
                    symbol = n.char
                    return symbol, low_count, high_count+n.count,mStep, total
                else:
                    low_count += n.count
                    high_count += n.count
        print "Error: value not found"

    # Decodieren
    def decode(self,input,orig=None):
        self.coder.out = input

        self.coder.input_len = len(input)
        self.coder.steps = len(input)/10
        self.coder.steps_perc = len(input)/100

        ctx = []
        currentNode = self.root
        escaped = False;
        self.coder.getBit(self.coder.buffersize)
        decmsg = ''
        declist = []
        
        excludeList = []
        counter = 0

        backupend = 0
        while True:
            symbol = None
            if self.exculsion and excludeList:
                symbol, low_count, high_count, mStep, total = self.excludeDecode(currentNode, excludeList)
                if total > currentNode.next('ESC').count:
                    self.coder.decode(low_count,high_count,mStep)
            else:
                value, mStep = self.coder.decode_target(currentNode.total)
                symbol, low_count, high_count = currentNode.findValue(value)
                self.coder.decode(low_count,high_count,mStep)

            #Beenden bei END Symbol
            if symbol == 'END':
                break
            if len(self.coder.out) == 0:
                backupend += 1
                if backupend == 20:
                    print "ACHTUNG! Der Text wurde enthält Fehler! Decodierung wurde abgebrochen!"
                    break

            # ESC Symbol verarbeiten
            elif symbol == 'ESC':
                escaped = True
                if not ctx:
                    currentNode = self.minus_ctx
                    excludeList = []
                else:
                    excludeList = list(set(currentNode.valueList + excludeList))
                    ctx = ctx[1:]
                    currentNode = self.path(ctx)

            # Normales Symbol verarbeiten
            else:
                # ExcludeList leeren
                excludeList = []
                # Integer Wert aus Kontext -1 verarbeiten
                if currentNode == self.minus_ctx:
                    #remove Symbol from minux context
                    self.updateMinusCtx(symbol)
                decmsg += symbol
                declist.append(symbol)
                # Fals orig gesetzt is Vergleich zwischen orginal und Decodiertem 
                if (not orig is None) and decmsg[counter] != orig[counter]:
                    print "Error: Decoded Char doesn't match Original"
                    print "-"*5+str(counter)+"-"*5
                    print 'orig: ' + orig[counter]
                    print 'dec: ' + decmsg[counter]
                    print 'low: ' + str(low_count)
                    print 'high: ' + str(high_count)
                    print 'total: ' + str(total)
                    print self.minus_ctx.nodeLists
                    print decmsg
                    return decmsg
                    break
                counter += 1
                # Kontext korrigieren
                ctx.append(symbol)
                if escaped:
                    ctx = declist[-self.ctx_len:]
                    escaped = False
                char_ctx = declist[-self.ctx_len-1:-1]
                self.addChar(char_ctx,symbol)
                if len(ctx) > self.ctx_len:
                    ctx = ctx[1:]
                currentNode = self.path(ctx)
            if currentNode == None:
                print 'ERROR: currentNode == None'
                break
        return decmsg

           
if __name__ == "__main__":
    ctx_len = 2
    lorem = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut l"
    lorem2 = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,"
    abab = "abc"*40
    lorem3 = "Lorem "*10
    msg = lorem2
    trie_ne = Trie(ctx_len)
    trie_e = Trie(ctx_len,True)
    print 'start encoding no exculsion'
    trie_ne.encode(msg)
    print 'start encoding with exculsion'
    trie_e.encode(msg)
    endcoded_ne = trie_ne.coder.out
    endcoded_e = trie_e.coder.out

    bpc_ne = float(len(endcoded_ne))/float(len(msg))
    bpc_e = float(len(endcoded_e))/float(len(msg))
    print('Encoded_len no exculsion: '+str(len(endcoded_ne)))
    print('Encoded_len with exculsion: '+str(len(endcoded_e)))
    print('bpc no exculsion:'+str(bpc_ne))
    print('bpc with exculsion:'+str(bpc_e))

    triedec_ne = Trie(ctx_len)
    triedec_e = Trie(ctx_len,True)
    print 'start decoding no exculsion'
    decoded_msg_ne = triedec_ne.decode(endcoded_ne,msg)
    print 'start decoding with exculsion'
    decoded_msg_e = triedec_e.decode(endcoded_e,msg)
    if decoded_msg_ne == msg:
       print 'no exculsion works'
    if decoded_msg_e == msg:
       print 'exculsion works'

