# -*- coding: utf-8 -*-

class Arith:
    def __init__(self):
        self.buffersize = 31
        self.mLow = 0
        self.mHigh = pow(2,self.buffersize)-1
        self.mBuffer = 0
        self.g_Half = (self.mHigh + 1) / 2
        self.mScale = 0
        self.g_FirstQuater = (self.g_Half) / 2
        self.g_ThirdQuater = self.g_FirstQuater * 3
        self.out = ''
        self.decmsg = ''
        self.input_len = 0
        self.steps = 0
        self.steps_perc = 0


    def lookUpSym(self,symbol):
        return self.s_table[symbol][0],self.s_table[symbol][1],self.total

    def lookUpVal(self,value):
        for key,s in self.s_table.iteritems():
            low = s[0]
            high = s[1]
            if value >= s[0] and value < s[1]:
                return key,s[0],s[1]

    def setBit(self,bit):
        self.out += str(bit)
     #  #print 'out: ' +  str(self.out)

    def getBit(self,count = 1):

        #print 'count: ' + str(count)
        #print 'mBuffer: '+str(self.mBuffer)
        #print 'mBuffer: '+bin(self.mBuffer)
        b = bin(self.mBuffer)[(2):].zfill(self.buffersize)
        #print 'b: '+b
        if self.out == '':
            tmp = b[1:]+'0'
        else:
            tmp = b[count:]+ self.out[:count]
        
        if len(tmp) != self.buffersize:
           print "WAAAAA"
           print len(tmp)
        #print 'tmp getBit: '+ tmp
        self.out = self.out[count:]
        self.mBuffer = int(tmp,2)
        if (self.input_len - len(self.out))%self.steps == 0:
            if len(self.out)/self.steps_perc != 0:
                print str(len(self.out)/self.steps_perc) + "% ..."
      # #print 'buffer: ' + tmp
      # #print 'out: ' + tmp + str(self.out)

    def encode(self,low_count, high_count, total):
        mStep = (self.mHigh - self.mLow + 1) / total
        self.mHigh = self.mLow + mStep * high_count - 1
        self.mLow = self.mLow + mStep * low_count
       #print 'low_count: ' + str(low_count)
       #print 'high_count: ' + str(high_count)
       #print 'total: ' + str(total)
       #print 'mStep: ' + str(mStep)
       #print 'mHigh: ' + str(self.mHigh)
       #print 'mLow: ' + str(self.mLow)

        if self.mHigh < self.mLow:
           #print 'WWWWWAAAAAAAAAAAAA'
            return 'WA'

        while (self.mHigh < self.g_Half) or (self.mLow >= self.g_Half):
            if( self.mHigh < self.g_Half): # E1
               #print 'enc E1'
                self.setBit(0)
                self.mLow = self.mLow * 2
                self.mHigh = self.mHigh * 2 + 1

                while self.mScale > 0:
                    self.setBit(1)
                    self.mScale -= 1
                    
               #print 'mHigh: ' + str(self.mHigh)
               #print 'mLow: ' + str(self.mLow)
                if self.mHigh == 65535 or self.mLow == 65535 :
                   #print 'AAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHH'
                    return

            elif(self.mLow >= self.g_Half): # E2
               #print 'enc E2'
                self.setBit(1)
                self.mLow = 2 * (self.mLow - self.g_Half)
                self.mHigh = 2 * (self.mHigh - self.g_Half) + 1

                while self.mScale > 0:
                    self.setBit(0)
                    self.mScale -= 1

               #print 'mHigh: ' + str(self.mHigh)
               #print 'mLow: ' + str(self.mLow)
                if self.mHigh == 65535 or self.mLow == 65535 :
                   #print 'AAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHH'
                    return
#                return
        while (self.g_FirstQuater <= self.mLow) and (self.mHigh < self.g_ThirdQuater):
           #print 'enc E3'
            self.mScale += 1
            self.mLow = 2 * (self.mLow - self.g_FirstQuater)
            self.mHigh = 2 * (self.mHigh - self.g_FirstQuater ) + 1

           #print 'mHigh: ' + str(self.mHigh)
           #print 'mLow: ' + str(self.mLow)

    def end_encode(self):
        self.setBit(1)
        return
        if self.mLow > self.g_FirstQuater:
            self.setBit(0)
            while self.mScale > 0:
                self.setBit(1)
                self.mScale -= 1
        else:
            self.setBit(1)
            while self.mScale > 0:
                self.setBit(0)
                self.mScale -= 1

    def decode_target(self,total):
       #print 'total' + str(total)
       #print 'mHigh: ' + str(self.mHigh)
       #print 'mLow: ' + str(self.mLow)
       #print 'mBuffer: ' + str(self.mBuffer)

        mStep = (self.mHigh - self.mLow + 1) / total
        value = (self.mBuffer - self.mLow) / mStep

       #print 'mStep: ' + str(mStep)
       #print 'value : ' + str(value)
        # hier mit "Tabelle" vergleichen und symbol ermitteln
        #symbol,low_count,high_count = self.lookUpVal(value)
        #print 'symbol: ' + str(symbol)
        #print 'mStep: ' + str(mStep)
        #self.decmsg += symbol
        # dann decode() mit ensprechnenden low,high
        #self.decode(low_count, high_count, mStep)
        return value, mStep

    def decode(self,low_count, high_count, mStep):
        self.mHigh = self.mLow + mStep * high_count - 1
        self.mLow = self.mLow + mStep * low_count
        
        #print 'mHigh: ' + str(self.mHigh)
        #print 'mLow: ' + str(self.mLow)
       #print 'low_count: ' + str(low_count)
       #print 'high_count: ' + str(high_count)
       ##print 'total: ' + str(total)
       #print 'mStep: ' + str(mStep)
       #print 'mHigh: ' + str(self.mHigh)
       #print 'mLow: ' + str(self.mLow)

        while (self.mHigh < self.g_Half) or (self.mLow >= self.g_Half):
            if( self.mHigh < self.g_Half): # E1
               #print 'dec E1'
                self.mLow = self.mLow * 2
                self.mHigh = self.mHigh * 2 + 1
                #self.mBuffer = 2 * self.mBuffer + self.getBit()
                #self.mBuffer = 2 * self.mBuffer 
                self.getBit()

               #print 'mHigh: ' + str(self.mHigh)
               #print 'mLow: ' + str(self.mLow)

            elif(self.mLow >= self.g_Half): # E2
               #print 'dec E2'
                self.mLow = 2 * (self.mLow - self.g_Half)
                self.mHigh = 2 * (self.mHigh - self.g_Half) + 1
                #self.mBuffer = 2 * (self.mBuffer - self.g_Half) + self.getBit()
                #self.mBuffer = 2 * (self.mBuffer - self.g_Half)
                self.mBuffer = (self.mBuffer - self.g_Half)
                self.getBit()

               #print 'mHigh: ' + str(self.mHigh)
               #print 'mLow: ' + str(self.mLow)

            self.mScale = 0

          #  if self.mBuffer == 64:
          #     return 

        while (self.g_FirstQuater <= self.mLow) and (self.mHigh < self.g_ThirdQuater):
           #print 'dec E3'
            self.mScale += 1
            self.mLow = 2 * (self.mLow - self.g_FirstQuater)
            self.mHigh = 2 * (self.mHigh - self.g_FirstQuater ) + 1
            #self.mBuffer = 2 * (self.mBuffer - self.g_FirstQuater) + self.getBit();
            #self.mBuffer = 2 * (self.mBuffer - self.g_FirstQuater) 
            self.mBuffer = (self.mBuffer - self.g_FirstQuater) 
            self.getBit();

           #print 'mHigh: ' + str(self.mHigh)
           #print 'mLow: ' + str(self.mLow)
         #   if self.mBuffer == 64:
         #       return

if __name__ == '__main__':
    #msg = 'abccedac'
    msg = 'abccdacababbbcabcbbabcbabbaaccc'
    msg_bin = ''.join(format(ord(x), 'b') for x in msg)
    stream = msg
    ar = Arith()
    while stream != '':
        next_sym = stream[:1]
        stream = stream[1:]
        low_count, high_count, total = ar.lookUpSym(next_sym)
        ar.encode(low_count, high_count, total)

       #print 'out: '+ar.out
       #print 'mScale: ' +str(ar.mScale)


    low_count, high_count, total = ar.lookUpSym('end')
    ar.encode(low_count, high_count, total)
    if ar.mLow < ar.g_FirstQuater:
        ar.setBit(0)
        while ar.mScale > 0:
            ar.setBit(1)
            ar.mScale -= 1
    else:
        ar.setBit(1)
        while ar.mScale > 0:
            ar.setBit(0)
            ar.mScale -= 1
   #print 'out :' + ar.out
   #print '00010101001101111'
   #print msg_bin
   #print len(ar.out)
   #print len(msg_bin)
    ardec = Arith()
    ardec.out = ar.out
   #print 'out: '+ardec.out
    ardec.getBit(7)
    decmsg = ardec.decode_target(ardec.total)
    #while ardec.out != '' and ardec.mBuffer != 64:
    
   #print 'symbol: ' + decmsg 
    while True:
       #print 'out: '+ ardec.out
        symbol = ardec.decode_target(ardec.total)
       #print 'symbol: ' + symbol
       #print 'dec msg: ' + decmsg
       #print 'mScale: ' +str(ardec.mScale)
       #print 'mBuffer: ' +str(ardec.mBuffer)
        if symbol == 'end':
            break
        else:
            decmsg += symbol
   #print decmsg
   #print msg
    if decmsg == msg:
       print 'Yeah'
        

   #print len(ar.out)
   #print len(msg_bin)
