# Mathematisches Praktikum in Codierung und Inforamtionstheorie

## Verwendung

### Codieren
`python Encoder.py [Filename] [Output-Filename]`

### Decodieren
`python Decoder.py [Filename] [Output-Filename] [Original-Filename]`

Original-Filename ist optional, um während dem Decodieren schon decodierungsfehler zu finden.

## Augenblicklicher Stand

> 2.26 Bits pro Zeichen

### Bisherige Verbesserungen

* Exklusion von Bereits ausgeschlossenen Zeichen in niedrigeren Kontexten
* Berechnung der Escape-Wahrscheinlichkeit aufgrund von der aktuellen Ordnung des Kontextes, der Anzahl der Zeichen im Aktuellen Kontext, der höchsten Zeichenhäufigkeit im aktuellen Kontext und der Anzahl der möglichen Zeichen
* Updaten des "Kontext -1" nachdem ein Zeichen daraus entnommen wurde
* 'th','Alice','said' (häufige Wörter) als ein Zeichen
